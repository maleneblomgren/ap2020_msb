# Living.Being
by Sofie Berg Petersen og Malene Storm Blomgren

![ScreenShot](MiniEx8.PNG)

RUNME: https://maleneblomgren.gitlab.io/ap2020_msb/MiniEx8

Sofies (if mine doesn't work) RUNME: https://sofiebp.gitlab.io/ap-2020/MiniEx8/

Repository: https://gitlab.com/maleneblomgren/ap2020_msb/-/tree/master/

**Describe your program in terms of how it works, and what you have used and learnt?**
Our program has a very space/alien like theme to it. We have chosen a simple color palette with black and yellow where we have used different opacities of the yellow. The background is black and further towards the middle of the canvas we have created circles on top of each other with different opacities of yellow. We made that choice to give the illusion of depth where the light becomes brighter towards the middle. On top of the canvas there’s ellipses in different sizes moving around randomly across the canvas to make the program more dynamic. In the middle we have placed an alien figure. In front of the alien figure are the text we have pulled from a JSON file we have created ourselves. The text rotates with 5 degrees from each other. The way it’s placed gives (like the circles) the illusion of depth with it being brightest in the middle of the canvas. It also gives the illusion of depth with the text going beyond the canvas. The text are a collection of thoughts we have put together. They relate to the alien like feeling many have, which we will comment on further on. The thoughts we have created are the following: 

- “to think as human to think as machine to obey your rules to find them unforeseen”

- “perceived not understood deceiving another fool an imposter in the midst in your pool of intellect”

- “data piling behind your sockets stuck inside your closing brackets unsure ground of flooded zeroes underneath my shaky breathing”

Finally we have used sound in our program. The audio voices is the same as the text/thoughts we have in the program and in our JSON file. The sound is very strange to listen to, it’s quite robotic and alien.  

For the code and the syntax, we tried creating variables with slightly meaningful or poetic names, but as for the rest of the code, we’ve only thought about functionality and not readability. It is not very poetic, it is simply functional as we wanted to focus more on the syntaxes, that we haven’t yet mastered completely and to learn them instead of trying to make our code poetic or vocable.

**Analyze and articulate your work:**
We’ve reflected upon voice in our RUNME in relation to alienation, intelligence and the imposter-syndrome. This is also relating to the text we read in Software Studies “Intelligence” by Andrew Goffey. The text talks amongst others about how we perceive intelligence as humans and what an alien would have to do, in order to convince us that it is intelligent. The text amongst others argues that we only perceive human-intelligence as intelligence, and have difficulty acknowledging intelligence in the non-human area. So an alien would have to simulate or show human-like intelligence to be perceived as intelligent. Yet the text refers to Avital Ronell, who made the observation that intelligence and stupidity can actually be mistaken for one another. If that is so, how can we know exactly what we define as intelligence, which would be “...“the ability to attain goals in the face of obstacles by means of decisions based on rational (truth- obeying) rules.”...” (- A software lexicon, Intelligence, page 134) (this is what the alien should do in order to convince us it is intelligent) how can we then still mistake stupidity and intelligence for one another? 
This is where we are sneaking in the imposter-syndrome aspect of our RUNME. The imposter-syndrome is where a persons negative thoughts makes them believe, that they are constantly tricking others in relation to what they are capable off. That they are for example not good enough for their job, they’re an imposter and they’ve tricked their boss into believing they’re good enough and it is only a matter of time before they are found out and fired. Likewise this is seen in students, doubting themselves, not thinking they’re actually good enough or smart enough for university, they’re tricking their peers, teachers or relying on luck whenever they do something good, smart or get good grades. 
How can we doubt our own intelligence when we’re even unsure on how to judge the intelligence of others? Intelligence is seen in many forms and expressions, just because one student is lacking in one area, doesn’t mean they’re generally stupid/unintelligent.

Looking back at the alien, wanting to convince us of it being intelligent, what if we look at it in relation to general alienation. We’ve most likely in our life experienced other people, peers, classmates being alienating in one way or another from the larger group. Lots of theories goes into why this happens, but perhaps the imposter syndrome can even be applied here. When doubting ones likability or value in social connection, whenever one gains friends the person will again feel like an imposter that is tricking their friends into liking them. That eventually they’ll see through them and found out that they’re actually not at all likable or worth being friends with. Can this also be the reason that people are alienated, because they eventually end up alienating themselves based upon what they believe people are thinking about them?

These are some of the thoughts that went into this RUNME. How we perceive intelligence. How we are unsure of our own intelligence or what it means to be intelligent. How alienation can be applied to more areas of ourselves, and what it means to feel like an imposter.

**Analyze your own e-lit work by using the text Vocable Code and/or The Aesthetic of Generative Code (or other texts that addresses code/voice/language) in relation to code and language**
The two texts Vocable Code and The Aesthetic of Generative Code, that we have been reading for this week's class, focus on code in a more poetic aspect. As mentioned our code isn’t entirely poetic as the examples we have seen throughout this week. We tried to create variables with slightly meaningful or poetic names, but the rest of the code focus more on the functionality of the program. We focused more on the syntaxes being able to execute the program in the way we wanted them to, rather than building it up in a poetic way. We believe that the deeper meaning and understanding of our code lies in the execution rather than how the code has been written. Perhaps a challenge would be to try and set it up differently to make it more poetic. We still tried to see, how we could connect the required reading and our program. In The Aesthetic of Generative Code Geoff Cox, Alex Mclean and Adrian Ward mentions in the beginning what their argument is in relation to poetry and aesthetic, which we feel relates well to our program, the written form is not simply what gives the aesthetic value. "Our argument is that, like poetry, the aesthetic value of code lies in its execution, not simply its written form"

The text also talks about senses. Here they bring an example of an apple “The taste of the apple…lies in the contact of the fruit  with the palate, not the fruit itself” The fruit itself is not the important part but the taste that comes in contact with the palate. It’s the same with the contact between poetry and the reader, that’s what’s important.

The beauty and the aesthetics of materials becomes very visible through the senses. When we code it’s mostly sight and sometimes hearing that plays a part of the experience. It’s difficult to incorporate smell, taste and feel. The experience that comes with our program is of course sight and hearing. You cannot taste anything, smell anything or feel anything physically. However we believe it talks to our inner feeling. With the combination of the visual and sound it boost the feeling of the theme of alienation we have chosen as mentioned above. 

The audio we have chosen to put into our program speaks to how messages can be perceived based on the voice(s) delivering them. The voice in ours is very difficult to put in a specific box, is it human, robotic, alien like? It doesn’t sound like a normal human talking, it sounds very strange to the ear. This fits perfectly to our vision of the program. It makes it easy to dehumanise a voice by making it sound like it’s from a different universe. With the manipulation of the tone to the voice in the audio it makes the text/thoughts seem even more negative. 

In Vocable Code an example that’s mentioned are the dadaist “sound poems” by German Kurt Schwitters. His output was expressed through an arrangement of objects and language focusing on vocables. It’s also mentioned that treating the vocables as data open for reassemblage, Schwitters explains the following:  

"The materials are not to be used logically in their objective relationships, but only within the logic of the work of art. The more intensively the work of art destroys rational objective logic, the greater the possibilities of artistic form. Just as in poetry words is played off against word, so in this instance one will play off factor against factor, material against material" p. 17

The more we go away from the rational objective logic of materials, the more they can unfold in a artistic form. With our text on the canvas we have broken the logical way we write and read text. It’s difficult to read everything that is written and is formed in a rather strange and chaotic way. The reason we have chosen to write it this was is because when one doubts his or hers intelligence and value it’s easy for the negative thoughts to fill up inside the head and become one big compact mass where you can’t separate them from each other. 

In Schwitters sound poem Ursonate (1922-1932) it doesn’t make sense to look at the poem in its written format, but the understanding of the poem makes more sense in the context of sound. With ours it may not make a lot of sense to read the text, you can’t read everything but the voice that runs in the background vocalizes the thoughts. Even though it’s strange as well to listen to, it ties everything together in the program. 

**How would you reflect upon various layers of voices and/or the performativity of code in your program?**
We have one voice, speaking poetry which is the same poetry as found in our JSON file. The voice is eerie, robotic, mysterious, alien. It should hopefully give an uncertainty of what exactly is meant to be speaking apart from it being a somewhat living being. Machine, human, alien? A lot of dehumanising steps has been made on the audio and therefore hopefully made it seem more alien. This is relating to the before-mentioned themes in our project. It is mainly oriented to make one reflect upon alienation as a theme, intelligence, whether or not this thing seems intelligent. To make the voice we used a voice generator, which reminded a lot of google translate and downloaded the file we got from that. Then we further manipulated the file in FL-studio which a music-making program. 

# References 
Geoffey, Andrew. “Intelligence”. Software Studies: A Lexicon. Ed. Matthew Fuller. Cambridge, Massachusetts: The MIT Press. 2008.

Geoff Cox, and Alex McLean. Speaking Code. Cambridge, Mass.: MIT Press, 2013

Geoff Cox, Alex McLean, and Adrian Ward. "The Aesthetics of Generative Code." Proc. of Generative Art. 2001.

