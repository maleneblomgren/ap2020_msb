![ScreenShot](ScreenshotMiniex1.png)

https://maleneblomgren.gitlab.io/ap2020_msb/Miniex1

//1. Although technically weren't my first coding experience, it was my first coding experience in a long time.
        It was nice to feel that I could understand what I was doing, I didn't just copy some code from an example. 
        I took a very simple syntax and used it a bunch, it was the randomizer, and it felt nice to learn about it slowly,
        rather than using a lot of different syntaxes that I didn't quite understand. Along the way I ran into issues, but
        I managed to locate and solve them. Often it was just a missing comma, but even a missing comma can be hard to find. 
        Earlier I felt that my only feelings towards programmings were frustration, because it never quite worked for me, now
        I feel like it's more doable. 
        
//2. Coding and writing is very different, atleast for me. I like writing especially fiction. Coding is nothing near writing for me. 
        Coding is issuing commands to a computer, in order to make it create and run something for you. Coding is more like communicating
        in a very specific langauge, were mistakes aren't tolerated. When writing normal text, you can make the same sentence in a thousand 
        different ways. You can't do that in programming. The difference in freedom of the two ways of writing, can be both claustophobic and 
        relieving at the same time. 
        When it comes to reading, normal text is usually understandable. Reading somebody else's code, can be impossible or atleast extremely difficult. 
        Reading code takes time, patience, focus and energy. Reading a book CAN be relaxing, unless it's an academic text, then it's more or less the
        same as with reading code. 
        
// 3. I definietly did not realize how important programming acutally is, before I read the texts. As for what it means to me, I'm not sure. I think
        it could quite easily become a lot more important to me, and already after this first excersise, I feel inspired to try out more things. 
        It gives me certain sense of freedom in the creative field, to be able to understand atleast some programming and to feel that I can actually learn it. 
        
//
