![ScreenShot](emoji2.png)

**Description**
The program is simply two stationary emojis, with no interaction available. One emoji is an depiction of a knife penetrating a phone, the other is a version of Stars'n'Stripes, with a gun on it, burning.
I have used triangle, quads, colours, lines, beginShape, endShape, vertex and most important of all; console.log(mouseX,mouseY). If I hadn't been able to use console log, 
I would never have been able to make the emojis. That was really nice to learn. Other than that, I think I've learnt something about geometrics and how important it is to 
consider them when trying to make an emoji. 

**Wider cultural context**
I hope that the wider culturals issues or context is pretty obvious in my emojis. The first emoji, which I call 'goodnight' or 'regulation method' is related to the issues of
addiction or frustration when dealing with social media. Social media is something that take up a lot of our energy, and it's been proven numerous times that it tends to make us sad.
So on that note, it'd be nice to have an emoji to encourage logging off or to state that social media ain't all that awesome. 
The second emoji is all about being anti-gun. It's meant to be provocative as, first of all, burning a countrys flag is quite offensive, but as long as they're not improving on their
gun politics, I think we all need an emoji to imaginarily/virtually burn Americas flag, as a hope that it would wake them up. 

URL: https://maleneblomgren.gitlab.io/ap2020_msb/MiniEx2 