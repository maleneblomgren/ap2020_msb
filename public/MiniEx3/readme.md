![ScreenShot](rock.png)

**What is your sketch? What do you want to explore and/or express?**
My throbber is a rock-paper-scissor-game you can play while waiting for something to load. It is very fast-paced though, so you should also be very quick. 
I thought that it was something to look at instead of just a hypnotising ring of something rotating or spinning around. Maybe it's something to keep you awake during long load
times or in the natural breaks that load time sometimes create. I actually wanted to draw the hands instead of just writing text, but because I've been sick, I ran out of time
and used it all to explore/get familiar with if-else statements, variables and arrays. Not sure I succeeded, but I did make something. 

**What are the time-related syntaxes/functions that you have used in your program, and why have you used them in this way?**
I used if-else syntax for a sense of time. The framerate is 1, so the count down is one word per second. I also used windowWidth, windowHeight, so that I were able to center the 
throbber nicely. 

**How is time being constructed in computation (refer to both the reading materials and your process of coding)?**
I'm not sure if this refers to the thing about the computational time, with the clock and the tick between something going from 0 to 1 and back to 0.. or something? 
The sense of time in my program is directly related to my framerate, yet the framerate is connected to my computers processors clock and its clocks ticks?

**Think about a throbber that you have encounted in digital culture, e.g. for streaming video on YouTube or loading the latest feeds on Facebook, or waiting for a ticket transaction, and consider what a throbber communicates, and/or hides? How might we characterise this icon differently?**
In video games throbbers are sometimes an animation of a characterfrom the game running. I encountered a very interesting throbber in, I think it were, Borderlands 2 or Pre-sequel(I'm not sure), where the throbber
were ClapTrap (a character from the game) running, and if you pressed the space-bar, he would jump. There weren't anything to jump over, but it was very neat to spend your waiting time 'interacting' with this
character. 
There is a bunch of different versions of the circular throbber, like a circular ring-like thing spinning, or slowly filling up and emptying again. I don't remember which program, but sometimes
when you encounter a circular throbber (or ring-like throbber) there is a sense of gravity inside it, so when it's coming from the top of the ring and going downwards to the bottom, 
it accelerates a little and then looses speed as it has to move upwards to the top again. These throbbers definietly feels more alive than more mechanical/stiff ones seen in earlier
software. The looped activity or running/working-feel in the throbbers are definietly important, when communicating that the software/program is working. Like you actively see something is
moving so that you can make the connection to the unseen work/loading the computer/software is doing.

URL: https://maleneblomgren.gitlab.io/ap2020_msb/MiniEx3 