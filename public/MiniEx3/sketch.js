

var x = 0

function setup(){
frameRate(1);
rectMode(CENTER)
textAlign(CENTER)

}

function draw(){
createCanvas(windowWidth, windowHeight);
  background(230);
  fill(1)
  textSize(50)

noStroke()
fill(230,10);
rect(windowWidth/2,windowHeight/2,windowWidth,windowHeight);

if (x==0) {

  fill(0,255,0);
  text('rock',windowWidth/2,windowHeight/2);
  x=x+1
}
  else if (x==1) {

    fill(0,255,0);
    text('paper',windowWidth/2,windowHeight/2)
    x=x+1
} else if (x==2) {

  fill(0,255,0);
  text('scissors',windowWidth/2,windowHeight/2);
  x=x+1

} else if (x==3){

  fill(255,0,0);
  let signs = ['ROCK','PAPER','SCISSORS']
  text(random(signs),windowWidth/2,windowHeight/2)
  x=x-3
}
}
