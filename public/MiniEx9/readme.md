## GIFingEmotional
### Link to program: https://sofiebp.gitlab.io/ap-2020/MiniEx9/
### Link to repository: https://gitlab.com/SofieBP/ap-2020/-/tree/master/public/MiniEx9
### Screenshot: ![ScreenShot](MiniEx9.PNG)
#### Who are you collaborating with / which group?
**Group 4:** Sofie Berg Pedersen @SofieBP , Mira Bella Dyring Morsø @MiraMorso, Malene Storm Blomgren @maleneblomgren & Emma Marie Kongsbak Bertelsen @K0ngsbak  


***What is the program about? which API have you used and why?***

Sometimes it is hard to put words to your feelings, and explain to people how you feel. But a picture says more than a thousand words, right? So a gif most at least say more than 10 thousand words. Math. With our program we wanted to help the user express their feelings in an easy way. 
We live in a time where we use “emotions” everywhere on social media in order to react on the different posts we scroll through every day. Looking at Facebook for instance, one can react with both a like, a heart, a sad face, a surprised face (wow) and an angry face. Emotions become a bigger and bigger part of social media. 
When thinking of how our program works, it is important to show some light on the reason why the program doesn’t refresh when you click a new button, but also on the reason why the position of the GIFs are random. First, the reason why the program doesn’t refresh is because of how one often feels more than just one emotion and we would like our program to express this. Second, the reason why the position of GIFs are random is because of how we believe that one cannot control one’s emotions - so why should we control the position? [We did try to make the GIFs appear in one specific location/position but after a lot of struggles with this, we decided that is was for the best and we are quite happy with our final result].


***Can you describe and reflect on your process of making this mini exercise in terms of acquiring, processing, using and representing data? How much do you understand this data or what do you want to know more about? How do platform providers sort the data and give you the selected data? What are the power-relations in the chosen APIs? What is the significance of APIs in digital culture?***

It was hard to figure out what we wanted to make, knowing that there are so many API’s out there, so in a way it was hard to choose because of all the opportunities available. After looking through some different API’s we figured that we wanted to create something where emotions played a role, and it seemed fitting using gifs to represent those different types of emotions we wanted to display in our program. 
For our miniex we’re not visualizing data in a way as much as we’re simply putting the raw data in the program for everybody to see. The gifs or other data we could acquire from the site or not processed. Of course we’ve selected to focus on only a handful of categories inside their vast collection of gifs. So we’re kind of using their search function within their API, but not further tagging specific data to value and presenting or visualising them in any specific way. 
One way that platform providers at least control their data, is through APIs keys and/or tokens. Where they can see how much you’ve gotten on stop you if you’ve exceeded your limit. They can see what you are using it for how and what exactly you are using. Take for example Google that will allow you to request up to a hundred pictures a day from their picture API, yet as you’ve reached the limit, you simply can’t request anymore. This is one of the power-relations at play. Asymmetrical exchange can definitely, be called into this as you as a user of Google provide the data for them, for example when uploading a picture to your blog, but when you try to access the data they’ve mined from you, they will either but limit to your access or simply deny if the data are of the category that is sold or simply of an intern database for their own use. 
At first we wanted every new gif to replace the older one, but decided against it. We decided that a trail of gifs fitting different moods would show, how complicated human feelings are. We cannot only feel one thing at a time, and it takes time to figure out what exactly you are feeling in tough situations. 


***Try to formulate a question in relation to web APIs or querying/parsing processes that you want to investigate further if you have more time.***

Can you really use APIs in the way that nag/net.art generator does, to put pressure on larger platforms/firms to gain further accessibility and/or transparency? Won’t larger platforms always disregard the smaller uproars against closed, un-transparent data-mining? They gain large sums of selling data and APIs they won’t just give that up. 

### References: 
**https://gitlab.com/Freyaw/miniex1?fbclid=IwAR1ywpOJ1vu5u12c9fJ9fPDgBox06h0AvdZj3ZCdjJHadCfOf0O9NpWjMwA
We used this sketch to figure out how to place our gifs on the canvas 
**https://gitlab.com/linesdmoller/ap2020/-/tree/master/public/MiniX9
We looked at Herborg, Linnea, Line and Lauras’ sketch to get inspiration to create the style of  our buttons, and changed them a bit to fit our own idea.

**https://www.dafont.com/blackbird.font
We used this link to download the font we wanted for our headline 

**https://www.youtube.com/watch?v=mj8_w11MvH8
We used Shiffman's’ video to understand how to use API data from giphy - and as our main inspiration
