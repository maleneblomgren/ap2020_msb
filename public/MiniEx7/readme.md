![ScreenShot](Blobber.png)

runme: https://maleneblomgren.gitlab.io/ap2020_msb/MiniEx7

**rules**
The rules of my generative program is that it starts, runs the movement for the objects and then gets stuck. The rules of the objects are that there are in total 12 objects. Six of them is assigned a random spot on the x-axis and the others are assigned a random spot on the y-axis. From that spot they're 'released' and falls their opposite sit of the canvas with an accelerating speed. I'm sure how to put it into a more boiled down rule-system than that. I said the framrate so they wouldn't fall too fast. They only thing going differently each time you refresh, is their starting point. It doesn't seem like much, put it creates a new piece each time.

I initially wanted to create the objects by using a for or while loop and an array so I didn't have to manually write out each object, but I couldn't figure it out. I also wanted to create something like Winnies tofu-spawner so that there would continue to come streams of colour through the canvas, yet I couldn't figure that out either, so I did not fulfill the task of creating at least one for or while loop. I gave up.

**role**
The role of rules and processes are not defined in my work in general. Whenever I go to the program with a rule, thought or task in mind, I never get to fulfill it and I end up frustrated instead. It is easier to start simple and slowly build something without any expectations although it won't always get your goal completed.

**readings**
I'm not sure I understand the random-syntax or autogenerator better now than I did before. I am still kind of confused as to how randomness in a computer works although I read the text. When we discussed in groups during the class, it also ended up becoming more abstract and weird, as if we were talking about the meaning of life or something. 

